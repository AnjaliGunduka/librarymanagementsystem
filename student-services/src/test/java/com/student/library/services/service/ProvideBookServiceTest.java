package com.student.library.services.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.student.library.services.client.BookServiceClient;
import com.student.library.services.entity.Book;
import com.student.library.services.entity.RequestBook;
import com.student.library.services.entity.Student;
import com.student.library.services.enums.RequestStatus;
import com.student.library.services.enums.Status;
import com.student.library.services.exception.BookNotProvidedException;
import com.student.library.services.exception.Constants;
import com.student.library.services.repository.BookRepository;
import com.student.library.services.repository.RequestRepository;
import com.student.library.services.request.ProvideBookRequest;

@ExtendWith(MockitoExtension.class)
public class ProvideBookServiceTest {
	@InjectMocks
	ProvideBookService provideBookService;
	@Mock
	BookRepository bookRepository;
	@Mock
	RequestRepository requestRepository;
	@Mock
	RequestBookService requestBookService;
	@Mock
	BookServiceClient bookServiceClient;
	@Mock
	AuthService authService;
	@Mock
	RequestServiceTest requestServiceTest;

	ProvideBookRequest provideBookRequest = new ProvideBookRequest(1L, 1L, new Date(2020 - 8 - 8), 0,
			RequestStatus.APPROVED);

	Student studentResponse = new Student(1L, "anjali", "anjali", "cse", 1, "12345", "anjali@gmail.com");


	Book book = new Book(1L, "core", "martin", Status.AVAILABLE, "anjali", "1", 8,  "computers");
	RequestBook requestBooks = new RequestBook(1L, "core", RequestStatus.REJECTED, "12345", 1, new Date(2020 - 8 - 8),
			studentResponse, book);
	RequestBook requestBook = new RequestBook(1L, "core", RequestStatus.REQUESTED, "12345", 1, new Date(2020 - 8 - 8),
			studentResponse, book);

	@Test
	public void testProvideBook() throws BookNotProvidedException {
		List<RequestBook> books = new ArrayList<RequestBook>();
		books.add(requestBook);
		when(requestBookService.getRequestBook(requestBook.getId())).thenReturn(requestBook);
		when(bookServiceClient.getBookById(" Any Token", book.getId())).thenReturn(book);
		when(authService.getAuthToken()).thenReturn(" Any Token");
		when(requestRepository.save(Mockito.any())).thenReturn(requestBook);
		assertThat(provideBookService.addReturnBook(provideBookRequest).getId()).isEqualTo(requestBook.getId());
	}

	@Test
	public void testtestProvideBookWithBookNotProvidedException() throws BookNotProvidedException {
		when(requestBookService.getRequestBook(requestBook.getId())).thenReturn(requestBooks);
		when(bookServiceClient.getBookById(" Any Token", book.getId())).thenReturn(book);
		when(authService.getAuthToken()).thenReturn(" Any Token");
		Exception exception = assertThrows(BookNotProvidedException.class,
				() -> provideBookService.addReturnBook(provideBookRequest));
		assertThat(Constants.INVALID_BOOK_REJECTED).isEqualTo(exception.getMessage());
	}

}
