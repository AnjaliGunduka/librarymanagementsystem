package com.library.service.book.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.service.book.entity.Book;
import com.library.service.book.exception.Constants;
import com.library.service.book.exception.NotFoundException;
import com.library.service.book.repository.BookRepository;

@Service
public class BookService {
	private static final Logger log = LoggerFactory.getLogger(BookService.class);

	@Autowired
	private BookRepository bookRepository;
	/**
	 * This method is used to Creates Book and saves in database
	 * 
	 * @param Book
	 * @return createdBook
	 */
	public Book addBook(Book book) {
		log.info("Book Created");
		return bookRepository.save(book);
	}
	
	/**
	 * This method is used to search books based on bookName
	 * 
	 * @param bookName
	 * @return
	 */

	public Book getBooksByBookName(String bookName) {
		return bookRepository.findByBookName(bookName)
				.orElseThrow(() -> new NotFoundException(Constants.BOOK_NOT_FOUND+bookName));
	}

	/**
	 * This method is used to search books based on bookId
	 * 
	 * @param id
	 * @return
	 */

	public Book getBookById(Long id) {
		return bookRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(Constants.BOOK_NOT_FOUND+id));
	}

	/**
	 * This method is to List All the Books available in DB
	 * 
	 * @return List
	 */

	public List<Book> getAllBooks() {
		log.info("Get All the Books");
		return bookRepository.findAll();

	}

	/**
	 * This method is used to update a Book details and save in DB
	 * 
	 * @param id
	 * @param book
	 * @return
	 */
	@Transactional
	public Book updateBook(Long id, Book book) {
		Book updatebook = bookRepository.getBookById(id);
		log.info("Book updated");
		updatebook.setId(id);
		updatebook.setBookName(book.getBookName());
		updatebook.setNumberOfCopies(book.getNumberOfCopies());
		return bookRepository.save(updatebook);
	}

	/**
	 * This method is used to delete Book Delete the Books
	 */
	@Transactional
	public String deleteBook(Long bookId) {
		log.info("Book deleted");
		if (bookId != null) {
			bookRepository.deleteById(bookId);
		}
		return "Book deleted";

	}

}
