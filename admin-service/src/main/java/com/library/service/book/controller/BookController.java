package com.library.service.book.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.library.service.book.entity.Book;
import com.library.service.book.entity.RequestBook;
import com.library.service.book.enums.RequestStatus;
import com.library.service.book.exception.BookNotReturnedException;
import com.library.service.book.exception.NoOfCopiesNotAvailableException;
import com.library.service.book.request.ProvideBookRequest;
import com.library.service.book.response.ProvideBookResponse;
import com.library.service.book.service.BookService;
import com.library.service.book.service.ProvideBookService;
import com.library.service.book.service.RequestBookService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/bookService")
public class BookController {
	@Autowired
	BookService bookService;

	@ApiOperation(value = "Creates a Book")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Book.class, message = "Book created Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters") })
	@PostMapping(value = "/addBook")
	public ResponseEntity<Book> addBook(@Valid @RequestBody Book book) {
		Book books = bookService.addBook(book);
		if (books != null) {
			return new ResponseEntity<Book>(books, HttpStatus.OK);
		}
		return new ResponseEntity<Book>(HttpStatus.BAD_REQUEST);
	}

	@ApiOperation(value = "Gets a Book By bookName")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Book.class, message = "Book Details fetched Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid Book bookName") })
	@GetMapping(value = "/book/{bookName}")
	public ResponseEntity<Book> getBooksByBookName(@Valid @PathVariable String bookName) {
		Book books = bookService.getBooksByBookName(bookName);
		if (books != null) {
			return new ResponseEntity<Book>(books, HttpStatus.OK);
		}
		return new ResponseEntity<Book>(HttpStatus.BAD_REQUEST);
	}

	@ApiOperation(value = "View Availabale Books")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Book.class, message = "All the Books Fetched Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters") })
	@GetMapping("/books")
	public ResponseEntity<List<Book>> getAllBooks() {
		List<Book> books = bookService.getAllBooks();
		return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
	}

	@ApiOperation(value = "Gets a Book By Id")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Book.class, message = "Book Details fetched Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid Book Id") })
	@GetMapping(value = "/books/{id}")
	public ResponseEntity<Book> getBookById(@Valid @PathVariable Long id) {
		Book books = bookService.getBookById(id);
		if (books != null) {
			return new ResponseEntity<Book>(books, HttpStatus.OK);
		}
		return new ResponseEntity<Book>(HttpStatus.BAD_REQUEST);
	}

	@ApiOperation(value = "Update a Book")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Book.class, message = "Book Details updated  Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters") })
	@PutMapping(value = "/updatebooks/{id}")
	public ResponseEntity<Book> updateBook(@Valid @PathVariable Long id, @RequestBody Book book) {
		Book books = bookService.updateBook(id, book);
		if (books != null) {
			return new ResponseEntity<Book>(books, HttpStatus.OK);
		}
		return new ResponseEntity<Book>(HttpStatus.BAD_REQUEST);
	}

	@ApiOperation(value = "Delete a Book")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Book.class, message = "Book Details updated  Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters") })
	@DeleteMapping(value = "/deletebooks/{bookId}")
	public ResponseEntity<String> deleteBook(@Valid @PathVariable Long bookId) {
		String book = bookService.deleteBook(bookId);
		if (!book.isEmpty()) {
			return new ResponseEntity<String>(book, HttpStatus.OK);
		}
		return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
	}

	@Autowired
	RequestBookService requestBookService;

	@ApiOperation(value = "View Requested Books")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = RequestBook.class, message = "All the Books Fetched Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters") })
	@GetMapping("/viewrequests")
	public ResponseEntity<List<RequestBook>> getAllRequestedBooks() {
		List<RequestBook> requestBook = requestBookService.getAllRequestedBooks();

		return new ResponseEntity<List<RequestBook>>(requestBook, HttpStatus.OK);
	}

	@ApiOperation(value = "search books based on studentCardno")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = RequestBook.class, message = "Request for book created Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters") })
	@GetMapping("/fetch/{cardNo}/viewdetails")
	public ResponseEntity<RequestBook> getRequestBookByCardNo(@Valid @PathVariable String cardNo) {
		RequestBook requestBook = requestBookService.getRequestBook(cardNo);
		if (requestBook != null) {
			return new ResponseEntity<RequestBook>(requestBook, HttpStatus.OK);
		}
		return new ResponseEntity<RequestBook>(HttpStatus.BAD_REQUEST);
	}

	@ApiOperation(value = "search requestbook based on for requestid")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = RequestBook.class, message = "Request for book created Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters") })
	@GetMapping("/{id}")
	public ResponseEntity<RequestBook> findByRequestId(@Valid @PathVariable Long id) {
		RequestBook requestBook = requestBookService.getRequestBook(id);
		if (requestBook != null) {
			return new ResponseEntity<RequestBook>(requestBook, HttpStatus.OK);
		}
		return new ResponseEntity<RequestBook>(HttpStatus.BAD_REQUEST);
	}

	@Autowired
	ProvideBookService provideBookService;

	@ApiOperation(value = "Creates an Provide Book")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = ProvideBookResponse.class, message = "Request for book created Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters") })
	@PostMapping(value = "/addprovidebook")
	public ResponseEntity<ProvideBookResponse> addProvideBook(@Valid @RequestBody ProvideBookRequest provideBookRequest)
			throws NoOfCopiesNotAvailableException {
		RequestBook requestBook = provideBookService.addProvideBook(provideBookRequest);
		ModelMapper modelMapper = new ModelMapper();
		if (provideBookRequest != null) {
			return new ResponseEntity<ProvideBookResponse>(modelMapper.map(requestBook, ProvideBookResponse.class),
					HttpStatus.CREATED);

		}
		return new ResponseEntity<ProvideBookResponse>(HttpStatus.BAD_REQUEST);
	}

	@ApiOperation(value = "Gets a Book By Status")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Book.class, message = "Book Details fetched Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid Book bookName") })
	@GetMapping(value = "/getBooksStatus/{status}")
	public ResponseEntity<RequestBook> getBooksStatus(@PathVariable RequestStatus status) {
		RequestBook requestBook = provideBookService.getBookStatus(status);
		if (requestBook != null) {
			return new ResponseEntity<RequestBook>(requestBook, HttpStatus.OK);
		}
		return new ResponseEntity<RequestBook>(HttpStatus.BAD_REQUEST);
	}
	@ApiOperation(value = "To Delete ReturnedBooks")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Book removed from RequestBook"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "Unable to remove Book from RequestBook") })
	@DeleteMapping(value = "/providedBook/delete", params = { "bookId", "requestBookId" })
	public ResponseEntity<String> deleteBookFromRequestBook(Long bookId, Long requestBookId) throws BookNotReturnedException  {
		String str = provideBookService.deleteBookFromRequestBook(bookId, requestBookId);
		if (str != null) {
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
	}
	@ApiOperation(value = "To calculateTotal ProvidedBooks")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Book removed from RequestBook"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "Unable to remove Book from RequestBook") })
	@GetMapping(value = "/providedBook/calculateTotalBooks",
	params = { "cardNo" }
	,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> calculateTotalProvidedBooks( @Valid String cardNo) {
		String str = provideBookService.calculateTotalProvidedBooks(cardNo);
		if (str != null) {
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
	}
	
}
