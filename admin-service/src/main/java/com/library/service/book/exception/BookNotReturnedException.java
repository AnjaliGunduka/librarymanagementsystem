package com.library.service.book.exception;

public class BookNotReturnedException extends Exception{
	private static final long serialVersionUID = 1L;

	public BookNotReturnedException(String message) {
		super(message);
	}
}
