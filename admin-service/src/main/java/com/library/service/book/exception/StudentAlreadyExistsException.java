package com.library.service.book.exception;

public class StudentAlreadyExistsException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public StudentAlreadyExistsException(String message) {
		// TODO Auto-generated constructor stub
		super(message);
	}

}
