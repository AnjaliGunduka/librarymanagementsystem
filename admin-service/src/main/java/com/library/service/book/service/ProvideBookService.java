package com.library.service.book.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.service.book.entity.Book;
import com.library.service.book.entity.RequestBook;
import com.library.service.book.entity.RequestBook$;
import com.library.service.book.enums.RequestStatus;
import com.library.service.book.exception.BookNotReturnedException;
import com.library.service.book.exception.Constants;
import com.library.service.book.exception.NoOfCopiesNotAvailableException;
import com.library.service.book.exception.NotFoundException;
import com.library.service.book.repository.BookRepository;
import com.library.service.book.repository.RequestRepository;
import com.library.service.book.request.ProvideBookRequest;
import com.speedment.jpastreamer.application.JPAStreamer;

@Service
public class ProvideBookService {

	@Autowired
	BookService bookService;
	@Autowired
	RequestBookService requestBookService;
	@Autowired
	RequestRepository requestRepository;
	@Autowired
	private BookRepository bookRepository;
	@Autowired
	private JPAStreamer jpaStreamer;

	/**
	 * This method is use to provide a book to student Based on BookId and StudentId
	 * And change requested status and date
	 * 
	 * @param provideBookRequest
	 * @throws NoOfCopiesNotAvailableException
	 * @return
	 */
	public RequestBook addProvideBook(ProvideBookRequest provideBookRequest) throws NoOfCopiesNotAvailableException {
		Book book = bookService.getBookById(provideBookRequest.getBookId());
		RequestBook requestBook = requestBookService.getRequestBook(provideBookRequest.getRequestBookId());
		requestBook.setStatus(provideBookRequest.getStatus());
		requestBook.setRequestDate(provideBookRequest.getRequestDate());
		int availableBooks = book.getNumberOfCopies();
		if (provideBookRequest.getNoOfBooks() > availableBooks) {
			throw new NoOfCopiesNotAvailableException(Constants.NOOFCOPIES_NOTAVAILABLE);
		}
		book.setNumberOfCopies(availableBooks - provideBookRequest.getNoOfBooks());
		provideBookRequest.setNoOfBooks(provideBookRequest.getNoOfBooks());
		bookRepository.save(book);
		return requestRepository.save(requestBook);

	}

	/**
	 * This Method is used to Count NoOfBooks Given To Students Search Based On
	 * RequestId
	 * 
	 * @param bookId
	 * @param requestBookId
	 * @return
	 */

	public String calculateTotalProvidedBooks(String cardNo) {
		int total = 0;
		RequestStatus status = RequestStatus.APPROVED;
		List<Book> books = new ArrayList<Book>();
		List<RequestBook> requestBookss = requestRepository.findBycardNoAndStatus(cardNo, status);
		for (RequestBook requestBook : requestBookss) {
			books.add(bookRepository.getBookById(requestBook.getBook().getId()));
		}
		List<RequestBook> requestBook = requestRepository.getBycardNo(cardNo);
		for (RequestBook requestBooks : requestBook) {
			total = total + requestBooks.getNoOfBooks();
		}
		return "Total no of books Provided To Student :- " + total + " Book name is :- " + books;
	}
	
	/**
	 * This method is used to delete the Book If the Book Is Returned
	 * 
	 * @param bookId
	 * @param requestBookId
	 * @return
	 * @throws BookNotReturnedException
	 */

	public String deleteBookFromRequestBook(Long bookId, Long requestBookId) throws BookNotReturnedException {
		String str = null;
		List<RequestBook> book = requestRepository.getBookById(requestBookId);
		for (RequestBook requestBook : book) {
			if (requestBook.getStatus() == RequestStatus.RETURNED) {
				requestRepository.deleteById(requestBook.getId());
				str = "Book Removed From Requested Books";
			} else {
				throw new BookNotReturnedException(Constants.BOOK_NOT_RETURNED + bookId);
			}
		}
		return str;
	}

	/***
	 * Search books based on the status approved rejected returned requested
	 * 
	 * @param status
	 * @return
	 */
	public RequestBook getBookStatus(RequestStatus status) {
		return jpaStreamer.stream(RequestBook.class).filter(RequestBook$.status.equal(status)).findFirst()
				.orElseThrow(() -> new NotFoundException(Constants.BOOK_NOT_FOUND));

	}

}
