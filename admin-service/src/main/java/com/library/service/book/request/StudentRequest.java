package com.library.service.book.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.library.service.book.validation.ValidPassword;

public class StudentRequest {
	@NotEmpty(message = "*Please provide your branch ")
	private String branch;
	@NotEmpty(message = "*Please provide your userName ")
	private String userName;
	@ValidPassword
	@NotEmpty(message = "*Please provide your password ")
	private String password;
	@NotEmpty(message = "*Please provide your Semester ")
	@Size(min = 1, max = 1, message = "Semester Length must be 1 characters")
	private int semester;
	@Email(message = "*Please provide your email ")
	private String email;
	
	
	public StudentRequest() {
		super();
	}

	public StudentRequest(@NotBlank(message = "Branch Name should not be null or Empty") String branch,
			@NotBlank(message = "User Name should not be null or Empty") String userName,
			@NotBlank(message = "Password should not be null or Empty") String password,
			@NotBlank(message = "Semester should not be null or Empty") @Size(min = 1, max = 1, message = "Semester Length must be 1 characters") int semester,
			String email) {
		super();
		this.branch = branch;
		this.userName = userName;
		this.password = password;
		this.semester = semester;
		this.email = email;
	}

	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

}
