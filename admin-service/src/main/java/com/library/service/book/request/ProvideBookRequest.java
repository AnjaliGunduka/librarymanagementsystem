package com.library.service.book.request;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.library.service.book.enums.RequestStatus;

public class ProvideBookRequest {

	@Min(value = 1, message = "Invalid Book Id")
	@NotNull(message = "Book Id should not be null")
	private Long bookId;
	@Min(value = 1, message = "Invalid RequestBook Id")
	@NotNull(message = "Request Book Id should not be null")
	private Long requestBookId;
	@JsonFormat(pattern = "DD-MM-yyyy", shape = Shape.STRING)
	private Date requestDate;
	private int noOfBooks;
	private RequestStatus status;

	public ProvideBookRequest(
			@Min(value = 1, message = "Invalid Book Id") @NotNull(message = "Book Id should not be null") Long bookId,
			@Min(value = 1, message = "Invalid RequestBook Id") @NotNull(message = "Request Book Id should not be null") Long requestBookId,
			@NotNull(message = "No of Books should not be null") int noOfBooks,
			@NotNull(message = "Status should not be null") RequestStatus status) {
		super();
		this.bookId = bookId;
		this.requestBookId = requestBookId;
		this.noOfBooks = noOfBooks;
		this.status = status;
	}

	

	public ProvideBookRequest(
			@Min(value = 1, message = "Invalid Book Id") @NotNull(message = "Book Id should not be null") Long bookId,
			@Min(value = 1, message = "Invalid RequestBook Id") @NotNull(message = "Request Book Id should not be null") Long requestBookId,
			Date requestDate, int noOfBooks, @NotNull(message = "Status should not be null") RequestStatus status) {
		super();
		this.bookId = bookId;
		this.requestBookId = requestBookId;
		this.requestDate = requestDate;
		this.noOfBooks = noOfBooks;
		this.status = status;
	}



	public Date getRequestDate() {
		return requestDate;
	}



	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}



	public ProvideBookRequest() {
		super();
	}

	public RequestStatus getStatus() {
		return status;
	}

	public void setStatus(RequestStatus status) {
		this.status = status;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Long getRequestBookId() {
		return requestBookId;
	}

	public void setRequestBookId(Long requestBookId) {
		this.requestBookId = requestBookId;
	}

	public int getNoOfBooks() {
		return noOfBooks;
	}

	public void setNoOfBooks(int noOfBooks) {
		this.noOfBooks = noOfBooks;
	}

}
