package com.library.service.book.mapper;


import org.springframework.stereotype.Service;

import com.library.service.book.entity.Student;
import com.library.service.book.request.StudentRequest;

@Service
public class StudentMapper {
	/**
	 * This method is used to map studentRequest and student
	 * @param studentRequest
	 * @return Student Instance
	 */

	public Student mapCreateStudent(StudentRequest studentRequest) {
		return new Student(generateCardNumber(studentRequest.getUserName()), studentRequest.getUserName(),
				studentRequest.getPassword(), studentRequest.getBranch(), studentRequest.getSemester(),
				studentRequest.getEmail());
	}

	/**
	 * This method is used to Generates Card Number based userName 
	 * 
	 * @param userName
	 * @return
	 */

	private String generateCardNumber(String userName) {
		return (String.valueOf(Math.abs(userName.hashCode()))).substring(0, 5);
	}
}
