package com.library.service.book.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.library.service.book.entity.RequestBook;
import com.library.service.book.entity.Student;
import com.library.service.book.enums.RequestStatus;



@Repository
public interface RequestRepository extends JpaRepository<RequestBook, Long> {
	
	Optional<RequestBook> findByIdAndStudent(Long requestBookId, Student student);
	Optional<RequestBook> findById(Long requestBookId);
	Optional<RequestBook> findBycardNo(String cardNo);
	List<RequestBook> getBookById(Long requestBookId);
	List<RequestBook> getRequestBookById(Long requestBookId);
	List<RequestBook> getBycardNo(String cardNo);
	@Query(value = "FROM RequestBook r WHERE r.cardNo=?1 AND  r.status=?2")
	List<RequestBook> findBycardNoAndStatus(String cardNo,RequestStatus status);
	
}
