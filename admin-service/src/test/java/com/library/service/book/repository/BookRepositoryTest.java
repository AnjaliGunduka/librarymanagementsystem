package com.library.service.book.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.library.service.book.entity.Book;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class BookRepositoryTest {
	@Autowired
	private BookRepository bookRepository;

	@Test
	void findById() {
		Book book = new Book();
		book.setId(1L);
		book = bookRepository.save(book);
		Book fetchedBook = bookRepository.findById(book.getId()).get();
		assertNotNull(fetchedBook);
	}

}
