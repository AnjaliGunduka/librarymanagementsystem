package com.library.service.book.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

import com.library.service.book.config.JwtAuthenticationEntryPoint;
import com.library.service.book.config.JwtTokenUtil;
import com.library.service.book.entity.ApplicationUser;
import com.library.service.book.enums.Role;

import com.library.service.book.model.JwtRequest;
import com.library.service.book.request.CreateUserRequest;
import com.library.service.book.response.JWTTokenResponse;
import com.library.service.book.response.UserResponse;
import com.library.service.book.service.CustomUserDetailsService;

@ExtendWith(MockitoExtension.class)
public class AdminLoginControllerTest {
	@Mock
	private JwtTokenUtil jwtTokenUtil;
	@Mock
	CustomUserDetailsService customUserDetailsService;
	@Mock
	JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	@Mock
	private AuthenticationManager authenticationManager;
	@InjectMocks
	AdminLoginController adminLoginController;

	private static UserDetails usedetails;

	ApplicationUser createApplicationUser = new ApplicationUser(1L, "admin", "admin", Role.ADMIN);
	CreateUserRequest createUserRequest = new CreateUserRequest("admin", "admin", "ADMIN");
	JwtRequest authenticationRequest = new JwtRequest("admin", "admin");
	JWTTokenResponse authenticationResponse = new JWTTokenResponse("anyToken", "Bearer", 30 * 60);

	@Test
	public void testCreateUser() {
		when(customUserDetailsService.createUser(createUserRequest)).thenReturn(createApplicationUser);
		ResponseEntity<UserResponse> response = adminLoginController.createUser(createUserRequest);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		ResponseEntity<UserResponse> res = adminLoginController.createUser(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

	@Test
	public void testLoadUserByUsername() throws Exception {
		when(customUserDetailsService.loadUserByUsername(createUserRequest.getUsername())).thenReturn(usedetails);
		ResponseEntity<?> response = adminLoginController.adminLogin(authenticationRequest);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		ResponseEntity<UserResponse> res = adminLoginController.createUser(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

	@Test
	public void testLoginReturnsJwt() throws Exception {
		when(jwtTokenUtil.generateToken(usedetails)).thenReturn("anyToken");
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
				createApplicationUser, null);
		Mockito.when(authenticationManager.authenticate(Mockito.any(UsernamePasswordAuthenticationToken.class)))
				.thenReturn(authentication);
		when(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
				createApplicationUser.getUsername(), createApplicationUser.getPassword()))).thenReturn(authentication);
		when(customUserDetailsService.loadUserByUsername(authenticationRequest.getUsername())).thenReturn(usedetails);
		ResponseEntity<?> response = adminLoginController.adminLogin(authenticationRequest);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		ResponseEntity<UserResponse> res = adminLoginController.createUser(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());

	}
}
