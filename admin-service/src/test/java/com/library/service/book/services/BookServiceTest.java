package com.library.service.book.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.library.service.book.entity.Book;
import com.library.service.book.enums.Status;
import com.library.service.book.exception.Constants;
import com.library.service.book.exception.NotFoundException;
import com.library.service.book.repository.BookRepository;
import com.library.service.book.service.BookService;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {
	@InjectMocks
	BookService bookService;
	@Mock
	BookRepository bookRepository;


	Book book = new Book(1L, "core", "martin", Status.AVAILABLE, "anjali", "1", 8, "computers");

	@Test
	public void testGetBookById() {
		when(bookRepository.findById(1L)).thenReturn(Optional.of(book));
		assertThat(bookService.getBookById(1L).getId()).isEqualTo(book.getId());

	}

	@Test
	public void testGetBookByBookName() {
		when(bookRepository.findByBookName(book.getBookName())).thenReturn(Optional.of(book));
		assertThat(bookService.getBooksByBookName(book.getBookName()).getBookName()).isEqualTo(book.getBookName());
	}

	@Test
	public void testgetAllBooks() {
		List<Book> books = new ArrayList<Book>();
		books.add(book);
		when(bookRepository.findAll()).thenReturn(books);
		assertThat(bookService.getAllBooks()).isEqualTo(books);

	}

	@Test
	public void testCreateBooks() {
		when(bookRepository.save(book)).thenReturn(book);
		assertThat(bookService.addBook(book).getId()).isEqualTo(book.getId());
	}

	Book bookss = new Book(1L, "cores", "martin", Status.AVAILABLE, "anjali", "1", 8, "computers");

	@Test
	public void testUpdateBook() {
		List<Book> books = new ArrayList<Book>();
		books.add(bookss);
		when(bookRepository.getBookById(bookss.getId())).thenReturn(bookss);
		bookss.setBookName("cores");
		when(bookRepository.save(bookss)).thenReturn(bookss);
		assertThat(bookService.updateBook(bookss.getId(), bookss)).isEqualTo(bookss);
	}

	@Test
	public void testGetBookByIdWithNotFoundException() throws NotFoundException {
		when(bookRepository.findById(1L)).thenReturn(Optional.empty());
		Exception exception = assertThrows(NotFoundException.class, () -> bookService.getBookById(1L));
		assertThat(Constants.BOOK_NOT_FOUND + book.getId()).isEqualTo(exception.getMessage());
	}

	@Test
	public void testGetBookByBookNameWithNotFoundException() throws NotFoundException {
		when(bookRepository.findByBookName("core")).thenReturn(Optional.empty());
		Exception exception = assertThrows(NotFoundException.class, () -> bookService.getBooksByBookName("core"));
		assertThat(Constants.BOOK_NOT_FOUND + book.getBookName()).isEqualTo(exception.getMessage());
	}

	@Test
	public void testdeleteBook() {
		assertEquals("Book deleted", bookService.deleteBook(book.getId()));
		verify(bookRepository, times(1)).deleteById(book.getId());
	}

}
