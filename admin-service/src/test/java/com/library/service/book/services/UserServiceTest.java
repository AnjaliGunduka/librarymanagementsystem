package com.library.service.book.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.library.service.book.config.JwtAuthenticationEntryPoint;
import com.library.service.book.config.JwtTokenUtil;
import com.library.service.book.entity.ApplicationUser;
import com.library.service.book.enums.Role;
import com.library.service.book.repository.UserRepository;
import com.library.service.book.request.CreateUserRequest;
import com.library.service.book.service.CustomUserDetailsService;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
	@Mock
	UserRepository userRepository;
	@InjectMocks
	private CustomUserDetailsService customUserDetailsService;
	@Mock
	private JwtTokenUtil jwtUtil;
	@Mock
	JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	@Mock
	PasswordEncoder passwordEncoder;
	private static UserDetails usedetails;

	@BeforeEach
	public void setUp() {
		usedetails = new User("user", "user", new ArrayList<>());
	}

	@Test
	void testCreateUser() {
		ApplicationUser applicationUser = new ApplicationUser(1L, "user", "test", Role.ADMIN);
		CreateUserRequest createdUserRequest = new CreateUserRequest("user", "test", "ADMIN");
		when(userRepository.save(Mockito.any())).thenReturn(applicationUser);
		assertThat(customUserDetailsService.createUser(createdUserRequest)).isEqualTo(applicationUser);
	}

	@Test
	void testCreateLoadUserByUsername() {
		ApplicationUser applicationUser = new ApplicationUser(1L, "user", "test", Role.ADMIN);
		when(userRepository.findByUsername("user")).thenReturn(Optional.of(applicationUser));
		assertThat(customUserDetailsService.loadUserByUsername(applicationUser.getUsername())).isEqualTo(usedetails);
	}

	@Test
	public void testGetLoadUserByUsernameWithNotFoundException() throws UsernameNotFoundException {
		ApplicationUser applicationUser = new ApplicationUser(1L, "user", "test", Role.ADMIN);
		when(userRepository.findByUsername(applicationUser.getUsername())).thenReturn(Optional.empty());
		Exception exception = assertThrows(UsernameNotFoundException.class,
				() -> customUserDetailsService.loadUserByUsername(applicationUser.getUsername()));
		assertThat("User not found with the Username " + applicationUser.getUsername())
				.isEqualTo(exception.getMessage());
	}
}
