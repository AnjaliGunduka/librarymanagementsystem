package com.library.service.book.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import com.library.service.book.entity.Book;
import com.library.service.book.entity.RequestBook;
import com.library.service.book.entity.Student;
import com.library.service.book.enums.RequestStatus;
import com.library.service.book.enums.Status;
import com.library.service.book.service.RequestBookService;

@ExtendWith(MockitoExtension.class)
public class RequestBookControllerTest {
	@Mock
	RequestBookService requestBookService;
	@InjectMocks
	BookController bookController;

	Student studentResponse = new Student(1L, "anjali", "anjali", "cse", 3, "12345", "anjali@gmail.com");


	Book book = new Book(1L, "core", "martin", Status.AVAILABLE, "anjali", "1", 8,  "computers");

	RequestBook requestBook = new RequestBook(3L, "core", RequestStatus.REQUESTED, "12345", 6, new Date(2020 - 8 - 8),
			studentResponse, book);

	@Test
	public void testgetAllRequestBooks() {
		List<RequestBook> requestBooks = new ArrayList<RequestBook>();
		requestBooks.add(requestBook);
		when(requestBookService.getAllRequestedBooks()).thenReturn(requestBooks);
		ResponseEntity<List<RequestBook>> response = bookController.getAllRequestedBooks();
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(requestBooks, response.getBody());
	}

	@Test
	public void testgetBookById() {
		when(requestBookService.getRequestBook(requestBook.getId())).thenReturn(requestBook);
		ResponseEntity<RequestBook> response = bookController.findByRequestId(requestBook.getId());
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(requestBook, response.getBody());
		ResponseEntity<RequestBook> res = bookController.findByRequestId(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

	@Test
	public void testRequestBookByCardno() {
		when(requestBookService.getRequestBook(requestBook.getCardNo())).thenReturn(requestBook);
		ResponseEntity<RequestBook> response = bookController.getRequestBookByCardNo(requestBook.getCardNo());
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(requestBook, response.getBody());
		ResponseEntity<RequestBook> res = bookController.getRequestBookByCardNo(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

}
