package com.library.service.book.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.library.service.book.entity.Book;
import com.library.service.book.entity.RequestBook;
import com.library.service.book.entity.Student;
import com.library.service.book.enums.RequestStatus;
import com.library.service.book.enums.Status;
import com.library.service.book.exception.BookNotReturnedException;
import com.library.service.book.exception.NoOfCopiesNotAvailableException;
import com.library.service.book.request.ProvideBookRequest;
import com.library.service.book.response.ProvideBookResponse;
import com.library.service.book.service.ProvideBookService;

@ExtendWith(MockitoExtension.class)
public class ProvideBookControllerTest {
	@Mock
	ProvideBookService provideBookService;
	@InjectMocks
	BookController bookController;

	ProvideBookRequest provideBookRequest = new ProvideBookRequest(1L, 1L, new Date(2020 - 8 - 8), 1,
			RequestStatus.APPROVED);
	Student student = new Student(1L, "anjali", "Anjali", "cse", 1, "12345", "anjali@gmail.com");

	Book book = new Book(1L, "core", "martin", Status.AVAILABLE, "anjali", "1", 8,  "computers");

	RequestBook requestBook = new RequestBook(3L, "core", RequestStatus.REQUESTED, "12345", 6, new Date(2020 - 8 - 8),
			student, book);

	@Test
	public void testProvideBookStatus() {
		RequestStatus status = RequestStatus.APPROVED;
		when(provideBookService.getBookStatus(status)).thenReturn(requestBook);
		ResponseEntity<RequestBook> response = bookController.getBooksStatus(status);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(requestBook, response.getBody());
		ResponseEntity<RequestBook> res = bookController.getBooksStatus(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

	@Test
	public void testaddProvideBook() throws NoOfCopiesNotAvailableException {
		when(provideBookService.addProvideBook(Mockito.any())).thenReturn(requestBook);
		ResponseEntity<ProvideBookResponse> response = bookController.addProvideBook(provideBookRequest);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		ResponseEntity<ProvideBookResponse> res = bookController.addProvideBook(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

	@Test
	public void testdeleteBookFromRequestBook() throws BookNotReturnedException {

		when(provideBookService.deleteBookFromRequestBook(requestBook.getId(), book.getId()))
				.thenReturn("Book Removed");

		ResponseEntity<String> response = bookController.deleteBookFromRequestBook(requestBook.getId(), book.getId());
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Book Removed", response.getBody());
		ResponseEntity<String> res = bookController.deleteBookFromRequestBook(null, null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

}
