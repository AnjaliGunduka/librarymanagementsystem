package com.library.service.book.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.library.service.book.entity.Book;
import com.library.service.book.enums.Status;
import com.library.service.book.service.BookService;

@ExtendWith(MockitoExtension.class)
public class BookControllerTest {
	@Mock
	BookService bookService;
	@InjectMocks
	BookController bookController;


	Book book = new Book(1L, "core", "martin", Status.AVAILABLE, "anjali", "1", 8,  "computers");

	@Test
	public void testCreateBook() {
		Book book = new Book(1L, "core", "martin", Status.AVAILABLE, "anjali", "1", 8,  "computers");
		when(bookService.addBook(book)).thenReturn(book);
		ResponseEntity<Book> response = bookController.addBook(book);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(book, response.getBody());

		ResponseEntity<Book> res = bookController.addBook(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

	@Test
	public void testgetAllBookss() {
		List<Book> books = new ArrayList<Book>();
		books.add(book);
		when(bookService.getAllBooks()).thenReturn(books);
		ResponseEntity<List<Book>> response = bookController.getAllBooks();
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(books, response.getBody());
	}

	@Test
	public void testUpdateBook() {
		Long bookId = 1L;
		when(bookService.updateBook(bookId, book)).thenReturn(book);
		ResponseEntity<Book> response = bookController.updateBook(bookId, book);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(book, response.getBody());
		ResponseEntity<Book> res = bookController.updateBook(null, null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

	@Test
	public void testDeleteBook() {
		when(bookService.deleteBook(book.getId())).thenReturn("Deleted");
		ResponseEntity<String> response = bookController.deleteBook(book.getId());
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Deleted", response.getBody());
	}

	@Test
	public void testgetBookById() {
		when(bookService.getBookById(book.getId())).thenReturn(book);
		ResponseEntity<Book> response = bookController.getBookById(book.getId());
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(book, response.getBody());
		ResponseEntity<Book> res = bookController.getBookById(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

	@Test
	public void testgetBookByBookName() {
		when(bookService.getBooksByBookName(book.getBookName())).thenReturn(book);
		ResponseEntity<Book> response = bookController.getBooksByBookName(book.getBookName());
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(book, response.getBody());
		ResponseEntity<Book> res = bookController.getBooksByBookName(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

}
