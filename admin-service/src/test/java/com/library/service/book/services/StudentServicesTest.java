package com.library.service.book.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.library.service.book.entity.Student;
import com.library.service.book.exception.Constants;
import com.library.service.book.exception.NotFoundException;
import com.library.service.book.mapper.StudentMapper;
import com.library.service.book.repository.StudentRepository;
import com.library.service.book.request.StudentRequest;
import com.library.service.book.service.StudentService;

@ExtendWith(MockitoExtension.class)
public class StudentServicesTest {
	@Mock
	StudentRepository studentRepository;
	@InjectMocks
	StudentService studentService;
	@Mock
	StudentMapper studentMapper;
	@Mock
	PasswordEncoder passwordEncoder;
	
	Student student = new Student(1L, "anjali", "anjali", "cse", 1, "12345", "anjali@gmail.com");

	StudentRequest studentRequest = new StudentRequest("cse", "anjali", "anjali", 1, "anjali@gmail.com");

	@Test
	public void testgetAllStudents() {
		List<Student> students = new ArrayList<Student>();
		students.add(student);
		Mockito.when(studentRepository.findAll()).thenReturn(students);
		assertEquals(students, studentService.getAllStudents());
	}

	@Test
	public void testGetStudentsById() {
		Mockito.when(studentRepository.findById(1L)).thenReturn(Optional.of(student));
		assertEquals(student.getId(), studentService.getStudent(1L).getId());

	}

	@Test
	public void testCreateUser() {
		Student student = new Student(1L, "anjali", "anjali", "cse", 1, "12345", "anjali@gmail.com");
		StudentRequest studentRequest = new StudentRequest("cse", "anjali", "anjali", 1, "anjali@gmail.com");
		when(studentRepository.save(Mockito.any())).thenReturn(student);
		assertThat(studentService.createStudent(studentRequest)).isEqualTo(student);
	}

	@Test
	public void testGetStudentsByIdWithNotFoundException() throws NotFoundException {
		when(studentRepository.findById(1L)).thenReturn(Optional.empty());
		Exception exception = assertThrows(NotFoundException.class, () -> studentService.getStudent(1L));
		assertThat(Constants.STUDENT_NOT_FOUND +student.getId()).isEqualTo(exception.getMessage());
	}
}
