package com.library.service.book.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.library.service.book.entity.Book;
import com.library.service.book.entity.RequestBook;
import com.library.service.book.entity.Student;
import com.library.service.book.enums.RequestStatus;
import com.library.service.book.enums.Status;
import com.library.service.book.exception.Constants;
import com.library.service.book.exception.NotFoundException;
import com.library.service.book.repository.RequestRepository;
import com.library.service.book.service.RequestBookService;

@ExtendWith(MockitoExtension.class)
public class RequestBookServiceTest {
	@Mock
	RequestRepository requestRepository;
	@InjectMocks
	RequestBookService requestBookService;

	Student studentResponse = new Student(1L, "anjali", "anjali", "cse", 3, "12345", "anjali@gmail.com");

	

	Book book = new Book(1L, "core", "martin", Status.AVAILABLE, "anjali", "1", 8, "computers");
	RequestBook requestBook = new RequestBook(1L, "core", RequestStatus.REQUESTED, "12345", 6, new Date(2020 - 8 - 8),
			studentResponse, book);

	@Test
	public void testgetAllRequestBooks() {
		List<RequestBook> requestBooks = new ArrayList<>();
		requestBooks.add(requestBook);
		Mockito.when(requestRepository.findAll()).thenReturn(requestBooks);
		assertEquals(requestBooks, requestBookService.getAllRequestedBooks());
	}

	@Test
	public void testGetRequestBooksById() {
		Mockito.when(requestRepository.findById(1L)).thenReturn(Optional.of(requestBook));
		assertEquals(requestBook.getId(), requestBookService.getRequestBook(1L).getId());

	}
	@Test
	public void testGetRequestBookByCardno() {
		Mockito.when(requestRepository.findBycardNo("12345")).thenReturn(Optional.of(requestBook));
		assertEquals(requestBook.getCardNo(), requestBookService.getRequestBook("12345").getCardNo());

	}
	@Test
	public void testGetRequestBookByCardnoWithNotFoundException() throws NotFoundException {
		when(requestRepository.findBycardNo("12345")).thenReturn(Optional.empty());
		Exception exception = assertThrows(NotFoundException.class, () -> requestBookService.getRequestBook("12345"));
		assertThat(Constants.REQUESTBOOK_NOT_FOUND+requestBook.getCardNo()).isEqualTo(exception.getMessage());
	}
	
	@Test
	public void testGetRequestBooksByIdWithNotFoundException() throws NotFoundException {
		when(requestRepository.findById(1L)).thenReturn(Optional.empty());
		Exception exception = assertThrows(NotFoundException.class, () -> requestBookService.getRequestBook(1L));
		assertThat(Constants.REQUESTBOOK_NOT_FOUND+requestBook.getId()).isEqualTo(exception.getMessage());
	}
}
