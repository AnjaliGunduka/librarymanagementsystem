package com.library.service.book.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.library.service.book.entity.Student;
import com.library.service.book.request.StudentRequest;
import com.library.service.book.response.StudentResponse;
import com.library.service.book.service.StudentService;

@ExtendWith(MockitoExtension.class)
public class StudentControllerTest {
	@Mock
	StudentService studentService;
	@InjectMocks
	StudentRegistrationController studentRegistrationController;

	Student student = new Student(1L, "anjali", "anjali", "cse", 1, "12345", "anjali@gmail.com");

	StudentRequest studentRequest = new StudentRequest("cse", "anjali", "anjali", 1, "anjali@gmail.com");

	@Test
	public void testCreateStudent() {
		when(studentService.createStudent(studentRequest)).thenReturn(student);
		ResponseEntity<StudentResponse> response = studentRegistrationController.createStudent(studentRequest);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(studentRequest.getBranch(), response.getBody().getBranch());
		ResponseEntity<StudentResponse> res = studentRegistrationController.createStudent(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}

	@Test
	public void testgetAllStudents() {
		List<Student> students = new ArrayList<>();
		students.add(student);
		when(studentService.getAllStudents()).thenReturn(students);
		ResponseEntity<List<Student>> response = studentRegistrationController.getAllStudents();
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(students, response.getBody());
	}
	@Test
	public void testStudentById() {
		when(studentService.getStudent(student.getId())).thenReturn(student);
		ResponseEntity<Student> response = studentRegistrationController.getStudent(student.getId());
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(student, response.getBody());
		ResponseEntity<Student> res = studentRegistrationController.getStudent(null);
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
		assertEquals(null, res.getBody());
	}
}



