package com.library.service.book.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.library.service.book.entity.Book;
import com.library.service.book.entity.RequestBook;
import com.library.service.book.entity.Student;
import com.library.service.book.enums.RequestStatus;
import com.library.service.book.enums.Status;
import com.library.service.book.exception.BookNotReturnedException;
import com.library.service.book.exception.Constants;
import com.library.service.book.exception.NoOfCopiesNotAvailableException;
import com.library.service.book.repository.BookRepository;
import com.library.service.book.repository.RequestRepository;
import com.library.service.book.request.ProvideBookRequest;
import com.library.service.book.service.BookService;
import com.library.service.book.service.ProvideBookService;
import com.library.service.book.service.RequestBookService;

@ExtendWith(MockitoExtension.class)
public class ProvideBookServiceTest {
	@InjectMocks
	ProvideBookService provideBookService;
	@Mock
	BookRepository bookRepository;
	@Mock
	RequestRepository requestRepository;
	@Mock
	RequestBookService requestBookService;
	@Mock
	BookService bookService;
	@Mock
	BookServiceTest bookServiceTest;

	ProvideBookRequest provideBookRequest = new ProvideBookRequest(1L, 1L, new Date(2020 - 8 - 8), 8,
			RequestStatus.APPROVED);
	ProvideBookRequest provideBookRequests = new ProvideBookRequest(1L, 1L, new Date(2020 - 8 - 8), 80,
			RequestStatus.APPROVED);
	Student student = new Student(1L, "anjali", "Anjali", "cse", 1, "12345", "anjali@gmail.com");


	Book book = new Book(1L, "core", "martin", Status.AVAILABLE, "anjali", "1", 8, "computers");

	RequestBook requestBook = new RequestBook(1L, "core", RequestStatus.REQUESTED, "12345", 5, new Date(2020 - 8 - 8),
			student, book);
	List<RequestBook> requestBookss = new ArrayList<RequestBook>();

	@Test
	public void testProvideBook() throws NoOfCopiesNotAvailableException {
		requestBook.setNoOfBooks(book.getNumberOfCopies());
		when(requestBookService.getRequestBook(requestBook.getId())).thenReturn(requestBook);
		when(bookService.getBookById(book.getId())).thenReturn(book);
		when(requestRepository.save(Mockito.any())).thenReturn(requestBook);
		assertThat(provideBookService.addProvideBook(provideBookRequest).getId()).isEqualTo(requestBook.getId());
	}

	@Test
	public void testProvideBookWithNoOfCopiesNotAvailableException() throws NoOfCopiesNotAvailableException {
		when(requestBookService.getRequestBook(requestBook.getId())).thenReturn(requestBook);
		when(bookService.getBookById(book.getId())).thenReturn(book);
		int availableBooks = book.getNumberOfCopies();
		assertEquals(provideBookRequest.getNoOfBooks(), availableBooks);
		Exception exception = assertThrows(NoOfCopiesNotAvailableException.class,
				() -> provideBookService.addProvideBook(provideBookRequests));
		assertThat(Constants.NOOFCOPIES_NOTAVAILABLE).isEqualTo(exception.getMessage());
	}

	RequestBook requestBooks = new RequestBook(1L, "core", RequestStatus.RETURNED, "12345", 5, new Date(2020 - 8 - 8),
			student, book);
	RequestBook requestBooksss = new RequestBook(1L, "core", RequestStatus.APPROVED, "12345", 5, new Date(2020 - 8 - 8),
			student, book);

	@Test
	public void testdeleteBookFromRequestBook() throws BookNotReturnedException {
		List<RequestBook> requestBookss = new ArrayList<RequestBook>();
		requestBookss.add(requestBooks);
		when(requestRepository.getBookById(Mockito.anyLong())).thenReturn(requestBookss);
		assertEquals("Book Removed From Requested Books",
				provideBookService.deleteBookFromRequestBook(requestBook.getId(), book.getId()));
		verify(requestRepository, times(1)).deleteById(Mockito.anyLong());
	}
	@Test
	public void testcalculateTotalProvidedBooks()  {
		List<RequestBook> requestBookss = new ArrayList<RequestBook>();
		requestBookss.add(requestBooks);
		int total = 0;
		List<Book> books = new ArrayList<Book>();
		books.add(book);
		when(bookRepository.getBookById(requestBook.getBook().getId())).thenReturn(book);
		when(requestRepository.findBycardNoAndStatus(requestBook.getCardNo(), requestBooksss.getStatus()))
		.thenReturn(requestBookss);
		assertThat(provideBookService.calculateTotalProvidedBooks(requestBook.getCardNo()))
		.isEqualTo("Total no of books Provided To Student :- " + total + "  Book name is :- " + books);
	}

	@Test
	public void testProvideBookWithBookNotReturnedException() throws BookNotReturnedException {
		List<RequestBook> requestBookss = new ArrayList<RequestBook>();
		requestBookss.add(requestBook);
		when(requestRepository.getBookById(Mockito.anyLong())).thenReturn(requestBookss);
		Exception exception = assertThrows(BookNotReturnedException.class,
				() -> provideBookService.deleteBookFromRequestBook(requestBook.getId(), book.getId()));
		assertThat(Constants.BOOK_NOT_RETURNED + book.getId()).isEqualTo(exception.getMessage());
	}

}
